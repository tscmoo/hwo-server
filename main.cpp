#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <cstdlib>
#include <cstdio>


#ifdef _WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#include <Windows.h>
#endif

#undef min
#undef max
//#undef near
//#undef far

#include <cmath>
#include <cstddef>
#include <cstring>

#include <array>
#include <vector>
#include <set>
#include <map>
#include <list>
#include <unordered_set>
#include <unordered_map>
#include <queue>
#include <functional>
#include <numeric>
#include <memory>
#include <random>
#include <chrono>
#include <thread>
#include <initializer_list>
#include <mutex>
#include <algorithm>
#include <utility>
#include <typeinfo>
using std::type_info;

#include <fstream>

#include <boost/asio.hpp>
#include <boost/asio/high_resolution_timer.hpp>
#include <boost/intrusive/set.hpp>

template<int=0,typename...args>
void xcept(args&&...v);

// template<typename T>
// struct alloc : std::allocator<T> {
// 	alloc() {}
// 	alloc(const alloc&other) : alloc() {}
// 	template<typename t>
// 	alloc(const alloc<t>&other) : alloc() {}
// 	template<typename t>
// 	struct rebind {
// 		typedef alloc<t> other;
// 	};
// };

#include "tsc/alloc.h"
using tsc::alloc;
#include "tsc/alloc_containers.h"

#include "tsc/strf.h"

struct simple_logger {
	std::mutex mut;
	tsc::a_string str, str2;
	bool newline;
	FILE*f;
	const char*filename;
	simple_logger() : newline(true), f(0), filename(nullptr) {
		filename = "log.txt";
	}
	template<typename...T>
	void operator()(const char*fmt,T&&...args) {
		std::lock_guard<std::mutex> lock(mut);
		if (!f && filename && *filename) f = fopen(filename,"w");
		const char*out_str;
		try {
			tsc::strf::format(str,fmt,std::forward<T>(args)...);
			if (newline) {
				time_t now = time(0);
				char tbuf[0x100];
				if (!strftime(tbuf,sizeof(tbuf),"%Y-%m-%d %H:%M:%S",localtime(&now))) tbuf[0] = 0;
				tsc::strf::format(str2,"%10s: %s",tbuf,str);
			}
			out_str = newline ? str2.c_str() : str.c_str();
		} catch (const std::exception&) {
			// log must never throw
			out_str = fmt;
		}
		
		newline = strchr(out_str,'\n') ? true : false;
		if (f) {
			fputs(out_str,f);
			fflush(f);
		}
		fputs(out_str,stdout);
	}
};
simple_logger logger;

enum {
	log_level_all,
	log_level_debug,
	log_level_info
};

int current_log_level = log_level_info;

template<typename...T>
void log(int level,const char*fmt,T&&...args) {
	if (current_log_level<=level) logger(fmt,std::forward<T>(args)...);
}

template<typename...T>
void log(const char*fmt,T&&...args) {
	log(log_level_debug,fmt,std::forward<T>(args)...);
}

struct xcept_t: std::exception {
	const char*str;
	xcept_t(const char*str) : str(str) {}
	virtual const char*what() const throw() {
		return str;
	}
};

struct xcept_wrapper_t {
	tsc::a_string str1, str2;
	int n;
	xcept_wrapper_t() {
		str1.reserve(0x100);
		str2.reserve(0x100);
		n = 0;
	}
	template<typename...T>
	void operator()(const char*fmt,T&&...args) {
		try {
			auto&str = ++n%2 ? str1 : str2;
			tsc::strf::format(str,fmt,std::forward<T>(args)...);
			log(log_level_info,"about to throw exception %s\n",str);
			throw xcept_t(str.c_str());
		} catch (const std::bad_alloc&) {
			throw xcept_t(fmt);
		}
	}
};
xcept_wrapper_t xcept_wrapper;
template<int,typename...args>
void xcept(args&&...v) {
	xcept_wrapper(std::forward<args>(v)...);
}

tsc::a_string format_string;
template<typename...T>
const char*format(const char*fmt,T&&...args) {
	return tsc::strf::format(format_string,fmt,std::forward<T>(args)...);
}


#include "tsc/high_resolution_timer.h"
#include "tsc/rng.h"

using tsc::rng;
using tsc::a_string;
using tsc::a_vector;
using tsc::a_deque;
using tsc::a_list;
using tsc::a_set;
using tsc::a_multiset;
using tsc::a_map;
using tsc::a_multimap;
using tsc::a_unordered_set;
using tsc::a_unordered_multiset;
using tsc::a_unordered_map;
using tsc::a_unordered_multimap;
//using tsc::a_circular_queue;


const double PI = 3.1415926535897932384626433;
//const double PI = 3.14159265358979;


#include "json.h"
#include "server.h"



void verify_test() {

	hwo::server srv;

	json_value jv;
	a_vector<a_vector<json_value>> server_messages, client_messages;
	auto add_dump = [&](const char*fn) {

		server_messages.emplace_back();
		client_messages.emplace_back();
		auto&my_server_messages = server_messages.back();
		auto&my_client_messages = client_messages.back();

		a_string game_id, car_name, car_color;
		FILE*f = fopen(fn,"r");
		if (!f) xcept("failed to open");
		char buf[0x10000];
		int n = 0;
		while (fgets(buf,sizeof(buf),f)) {

			jv.parse(buf,strlen(buf));

			++n;
			if (jv) {
				if (jv["msgType"]=="gameInit") {
					srv.set_track(jv["data"]["race"]["track"]);
				} else if (jv["msgType"]=="yourCar") {
					game_id = (a_string)jv["gameId"];
					car_name = (a_string)jv["data"]["name"];
					car_color = (a_string)jv["data"]["color"];
				}
				if (jv["msgType"]=="joinRace") {

				} else {
					if (n%2) my_server_messages.push_back(jv);
					else my_client_messages.push_back(jv);
				}
			}

		}
		fclose(f);

		if (!srv.game_id.empty() && srv.game_id!=game_id) xcept("mismatching game id"); 

		srv.game_id = game_id;
		srv.add_car(car_name,car_color);

	};

	add_dump("test1.txt");
	add_dump("test2.txt");
	add_dump("test3.txt");
	add_dump("test4.txt");
	add_dump("test5.txt");
	//add_dump("test.txt");

	json_value replay_messages;
	{
		FILE*f = fopen("replay.txt","r");
		if (!f) xcept("failed to open replay");
		fseek(f,0,SEEK_END);
		long fs = ftell(f);
		fseek(f,0,SEEK_SET);
		a_vector<char> buf;
		buf.resize(fs);
		if (!fread((char*)buf.data(),buf.size(),1,f)) xcept("read failed");
		fclose(f);
		replay_messages.parse(buf.data(),buf.size());
	}

	for (size_t i=0;i<server_messages.size();++i) {
		log("%d: %d server messages, %d client messages\n",i,server_messages[i].size(),client_messages[i].size());
	}

	auto verify_json = [&](const json_value&root_a,const json_value&root_b) {
		std::function<void(const json_value&,const json_value&,const a_string&)> visit = [&](const json_value&a,const json_value&b,const a_string&path) {
			if (a.type!=b.type) {
				log("theirs is - %s\n",root_a.dump());
				log("mine   is - %s\n",root_b.dump());
				xcept("type mismatch in %s\n",path);
			}
			if (a.type==json_value::t_number) {
				double diff = std::abs(a.n-b.n);
				if (diff>1e-8 && std::abs(diff-360)>1e-8) {
					log("theirs is - %s\n",root_a.dump());
					log("mine   is - %s\n",root_b.dump());
					xcept("mismatch in number field %s - %.16g vs %.16g",path,a.n,b.n);
				}
			} else if (a.type==json_value::t_string) {
				if (a.s!=b.s) {
					log("theirs is - %s\n",root_a.dump());
					log("mine   is - %s\n",root_b.dump());
					xcept("mismatch in string field %s - '%s' vs '%s'",path,a.s,b.s);
				}
			} else if (a.type==json_value::t_boolean) {
				if (a.b!=b.b) {
					log("theirs is - %s\n",root_a.dump());
					log("mine   is - %s\n",root_b.dump());
					xcept("mismatch in boolean field %s - %d vs %d",path,a.b,b.b);
				}
			} else if (a.type==json_value::t_array) {
				if (a.vector.size()!=b.vector.size()) {
					log("theirs is - %s\n",root_a.dump());
					log("mine   is - %s\n",root_b.dump());
					xcept("mismatch in array field %s - length %d vs %d",path,a.vector.size(),b.vector.size());
				}
				for (size_t i=0;i<a.vector.size();++i) {
					visit(a.vector[i],b.vector[i],path + format("[%d]",i));
				}
			} else if (a.type==json_value::t_object) {
				if (a.map.size()!=b.map.size()) {
					log("theirs is - %s\n",root_a.dump());
					log("mine   is - %s\n",root_b.dump());
					xcept("mismatch in object field %s - length %d vs %d",path,a.map.size(),b.map.size());
				}
				{
					auto i = a.map.find("msgType");
					if (i!=a.map.end() && i->second=="carVelocities") return;
				}
				for (auto&v : a.map) {
					auto i = b.map.find(v.first);
					if (i==b.map.end()) xcept("mismatch in object field %s - field %s does not exist",path,v.first);
					if (v.first=="millis") continue;
					if (v.first=="prevCommandTick") continue;
					visit(v.second,i->second,path + "." + v.first);
				}
			}
		};
		visit(root_a,root_b,"/");
	};

	a_vector<size_t> next_server_message = 0;
	a_vector<size_t> next_client_message = 0;
	next_server_message.resize(server_messages.size());
	next_client_message.resize(client_messages.size());
	auto server_message = [&](size_t to,const json_value&v){
		if (to==-1) {
			for (size_t i=0;i<server_messages.size();++i) {
				if (next_server_message[i]>=server_messages[i].size()) xcept("no more server messages");
				verify_json(server_messages[i][next_server_message[i]++],v);
			}
		} else {
			if (next_server_message.at(to)>=server_messages.at(to).size()) xcept("no more server messages");
			verify_json(server_messages.at(to)[next_server_message[to]++],v);
		}
		log("message %s ok\n",(a_string)v.map.find("msgType")->second);
	};
	auto client_message = [&]() {
		for (size_t i=0;i<client_messages.size();++i) {
			if (next_client_message[i]>=client_messages[i].size()) {
			} else srv.client_message(i,client_messages[i][next_client_message[i]++]);
		}
	};
	size_t next_replay_message = 0;
	auto replay_message = [&](const json_value&v) {
		return;
		if (v.map.find("msgType")->second=="liveRanking") return;
		auto rmsg = replay_messages[next_replay_message++];
		while (rmsg["msgType"]=="liveRanking") rmsg = replay_messages[next_replay_message++];
		verify_json(rmsg,v);
	};
	srv.start(server_message,replay_message);
	client_message();

	while (srv.run(server_message,replay_message)) {
		client_message();
	}
}

struct server_t;
struct matchmaking_t;
struct client_t {
	server_t*server;
	matchmaking_t*matchmaking;
	size_t car_index;
	a_vector<char> recv_buffer, send_buffer;
	double last_recv, last_send;
	bool received_message;
	boost::asio::ip::tcp::socket socket;
	boost::asio::ip::tcp::endpoint endpoint;
	a_string address_string;
	int async_count;
	int client_id;
	a_list<client_t>::iterator my_it;
	double next_timeout;
	boost::intrusive::set_member_hook<> timeout_hook;
	client_t(boost::asio::ip::tcp::socket socket) : socket(std::move(socket)), server(nullptr), matchmaking(nullptr), received_message(false), async_count(0) {}
};

struct client_timeout_less {
	bool operator()(const client_t&a,const client_t&b) const {
		return a.next_timeout < b.next_timeout;
	}
};

struct replay_file_writer {
	std::ofstream file;
	bool first;
	replay_file_writer() : first(true) {}
	void open(const char*fn) {
		file.open(fn);
		first = true;
		file << "[";
	}
	replay_file_writer&operator<<(const a_string&str) {
		if (!first) file << ",";
		file << str;
		first = false;
		return *this;
	}
	~replay_file_writer() {
		if (file.is_open()) file << "]";
	}
};

struct server_t {
	hwo::server srv;
	a_vector<client_t*> clients;
	a_list<server_t>::iterator my_it;
	replay_file_writer replay_file;
};

struct matchmaking_t {
	size_t car_count;
	a_string password;
	a_string track_name;
	a_vector<std::tuple<client_t*,a_string>> clients;
	json_value track_jv;
};

template<typename T,typename release_F>
struct async_handle_t {
	T&obj;
	release_F release_f;
	async_handle_t(T&obj,release_F&&release_f) : obj(obj), release_f(release_f) {
		++obj.async_count;
	}
	~async_handle_t() {
		if (!--obj.async_count) release_f(obj);
	}
	async_handle_t(const async_handle_t&n) : obj(n.obj), release_f(n.release_f) {
		++obj.async_count;
	}
	operator T&() const {
		return obj;
	}
	T&get() const {
		return obj;
	}
};
template<typename T,typename release_F>
async_handle_t<T,release_F> async_handle(T&obj,release_F&&f) {
	return async_handle_t<T,release_F>(obj,std::forward<release_F>(f));
}

template<typename F>
struct scope_exit_t {
	F f;
	scope_exit_t(F&&f) : f(std::forward<F>(f)) {}
	~scope_exit_t() {
		f();
	}
};
template<typename F>
scope_exit_t<F> scope_exit(F&&f) {
	return scope_exit_t<F>(std::forward<F>(f));
}

void net_server() {


	using boost::asio::ip::tcp;

	bool stop = false;

	a_list<client_t> clients;
	a_list<server_t> servers;

	a_list<matchmaking_t> matchmaking;

	tsc::high_resolution_timer ht;

	std::function<void(client_t&,const a_string&)> client_send_string;

	using namespace boost::intrusive;
	multiset<client_t,member_hook<client_t,set_member_hook<>,&client_t::timeout_hook>,compare<client_timeout_less>> timeout_set;

	const double timeout = 10.0;
	const double join_timeout = 60.0*5;

	std::function<void()> update_timer;

	server_t*cur_s;
	auto server_message = [&](size_t to,const json_value&jv){
		//log("send to %d: '%s'\n",to,jv.dump());
		if (to==-1) {
			for (auto*c : cur_s->clients) {
				if (!c) continue;
				client_send_string(*c,jv.dump()+"\n");
			}
		} else {
			if (cur_s->clients[to]) {
				client_send_string(*cur_s->clients[to],jv.dump()+"\n");
			}
		}
	};
	auto replay_message = [&](const json_value&jv) {
		cur_s->replay_file << jv.dump();
	};


	auto matchmaking_start = [&](matchmaking_t&mm) {
		
		auto mm_closer = scope_exit([&]{
			for (size_t i=0;i<mm.clients.size();++i) {
				auto&v = mm.clients[i];
				auto&c = *std::get<0>(v);
				c.matchmaking = nullptr;
				if (!c.server && c.socket.is_open()) c.socket.close();
			}
			for (auto i=matchmaking.begin();i!=matchmaking.end();++i) {
				if (&*i==&mm) {
					matchmaking.erase(i);
					break;
				}
			}
		});
		
		servers.emplace_back();
		auto&s = servers.back();
		s.my_it = --servers.end();
		auto&srv = s.srv;
		std::array<uint8_t,16> uuid;
		for (auto&v : uuid) v = tsc::rng(0x100);
		uuid[6] &= ~(0xf<<4);
		uuid[6] |= 4<<4;
		uuid[8] &= ~(3<<6);
		uuid[8] |= 2<<6;
		srv.game_id.resize(36);
		for (size_t i=0,idx=0;i<36;++i) {
			char*c = &srv.game_id[i];
			if (i==8 || i==13 || i==18 || i==23) *c = '-';
			else {
				c[0] = '0' + (uuid[idx]>>4);
				if (c[0]>'9') c[0] += 'a'-'9'-1;
				c[1] = '0' + (uuid[idx]&0xf);
				if (c[1]>'9') c[1] += 'a'-'9'-1;
				++i, ++idx;
			}
		}
		for (size_t i=0;i<mm.clients.size();++i) {
			auto&v = mm.clients[i];
			auto&c = *std::get<0>(v);
			c.matchmaking = nullptr;
			c.server = &s;
			c.car_index = i;
			s.clients.push_back(std::get<0>(v));

			const char*color;
			if (i==0) color = "red";
			else if (i==1) color = "green";
			else if (i==2) color = "blue";
			else if (i==3) color = "yellow";
			else if (i==4) color = "purple";
			else if (i==5) color = "orange";
			else color = format("color%d",i);

			srv.add_car(std::get<1>(v),color);
		}

		srv.set_track(mm.track_jv);
		
		s.replay_file.open(("recordings/" + srv.game_id + ".json").c_str());

		cur_s = &s;
		srv.start(server_message,replay_message);

		log("game started: id %s, cars %d, track %s (%s)\n",srv.game_id,srv.cars.size(),srv.track.id,srv.track.name);

		log("there are now %d servers and %d clients\n",servers.size(),clients.size());

	};

	auto server_shutdown = [&]() /* noexcept */ {
		for (auto*c : cur_s->clients) {
			if (!c) continue;
			if (!c->socket.is_open()) continue;
			boost::system::error_code ec;
			log("shutdown client %d (%s)\n",c->client_id,c->address_string);
			c->socket.shutdown(tcp::socket::shutdown_receive,ec);
			if (ec) {
				if (c->socket.is_open()) c->socket.close();
			}
		}
	};

	auto server_run = [&]() /* noexcept */ {
		bool okay = true;
		for (auto*c : cur_s->clients) {
			if (!c) continue;
			if (!c->received_message) {
				okay = false;
				break;
			}
		}
		if (okay) {
			for (auto*c : cur_s->clients) {
				if (!c) continue;
				c->received_message = false;
			}
			try {
				if (!cur_s->srv.run(server_message,replay_message)) {
					log("game done\n");
					server_shutdown();
				}
			} catch (const std::exception&e) {
				log("server: exception %s\n",e.what());
				server_shutdown();
			}
		}
	};

	auto on_client_message = [&](client_t&c,json_value&jv) {

		if (!c.server) {
			if (c.matchmaking) xcept("already in matchmaking");
			const a_string&msg_type = jv["msgType"];
			if (msg_type=="joinRace") {
				int car_count = jv["data"]["carCount"];
				const a_string&name = jv["data"]["botId"]["name"];
				const a_string&password = jv["data"]["password"];
				const a_string&track_name = jv["data"]["trackName"];

				matchmaking_t*mm = nullptr;
				for (auto&v : matchmaking) {
					if (car_count==v.car_count && password==v.password && track_name==v.track_name) {
						for (auto&v2 : v.clients) {
							if (std::get<1>(v2)==name) xcept("name already taken");
						}
						mm = &v;
						break;
					}
				}
				if (!mm) {
					if (matchmaking.size()>=0x100) xcept("too many matchmakings!");
					matchmaking.emplace_back();
					mm = &matchmaking.back();
					auto mm_cleanup = scope_exit([&]{
						if (mm->clients.empty()) {
							matchmaking.pop_back();
						}
					});
					mm->car_count = car_count;
					mm->password = password;
					mm->track_name = track_name;
					{
						const char*c = track_name.c_str();
						while (*c) {
							if ((*c>='A' && *c<='Z') ||
								(*c>='a' && *c<='z') ||
								(*c>='0' && *c<='9')) ++c;
							else xcept("invalid track name '%s'",track_name);
						}
					}
					{
						a_string fn = "tracks/" + track_name + ".json";
						std::ifstream f(fn.c_str(),std::ifstream::binary);
						if (!f) xcept("failed to open '%s'",fn);
						f.seekg(0, std::ifstream::end);
						size_t s = (size_t)f.tellg();
						f.seekg(0, std::ifstream::beg);
						a_vector<char> buf;
						buf.resize(s);
						f.read(buf.data(),buf.size());
						mm->track_jv.parse(buf.data(),buf.size());
					}
					mm->clients.emplace_back(&c,name);
				} else mm->clients.emplace_back(&c,name);
				c.matchmaking = mm;
				if (mm->clients.size()==mm->car_count) matchmaking_start(*mm);
				else {
					timeout_set.erase(c);
					c.next_timeout = ht.elapsed() + join_timeout;
					timeout_set.insert(c);
					update_timer();
				}

				log("there are now %d matchmakings\n",matchmaking.size());

			} else xcept("got unknown message type (%s)",msg_type);
		} else {
			c.server->srv.client_message(c.car_index,jv);

			timeout_set.erase(c);
			c.next_timeout = std::numeric_limits<double>::infinity();
			timeout_set.insert(c);
			update_timer();
			c.last_recv = c.last_send = ht.elapsed();
			c.received_message = true;
			cur_s = c.server;
			server_run();

		}

	};

	auto async_release = [&](client_t&c) /* noexcept */{
		log("client %d (%s) disconnected\n",c.client_id,c.address_string);
		timeout_set.erase(c);
		if (c.server) {
			if (c.server->srv.st.cars.size()>c.car_index) c.server->srv.client_dnf(c.car_index);
			c.server->clients[c.car_index] = nullptr;
			bool okay = false;
			for (auto*c2 : c.server->clients) {
				if (c2) {
					okay = true;
					break;
				}
			}
			if (!okay) {
				log("no more clients left, closing server\n");
				servers.erase(c.server->my_it);
			} else {
				cur_s = c.server;
				server_run();
			}
		}
		if (c.matchmaking) {
			for (auto i=c.matchmaking->clients.begin();i!=c.matchmaking->clients.end();++i) {
				if (std::get<0>(*i)==&c) {
					c.matchmaking->clients.erase(i);
					break;
				}
			}
			if (c.matchmaking->clients.empty()) {
				for (auto i=matchmaking.begin();i!=matchmaking.end();++i) {
					if (&*i==c.matchmaking) {
						matchmaking.erase(i);
						break;
					}
				}
			}
			log("there are now %d matchmakings\n",matchmaking.size());
		}
		clients.erase(c.my_it);
		log("there are now %d servers and %d clients\n",servers.size(),clients.size());
	};

	std::function<void(client_t&c,const boost::system::error_code&,size_t)> write_handler = [&](client_t&c,const boost::system::error_code&ec,size_t bytes_transferred) {
		
		try {

			if (!ec) {

				c.send_buffer.erase(c.send_buffer.begin(),c.send_buffer.begin()+bytes_transferred);

				if (!c.send_buffer.empty()) {
					using namespace std::placeholders;
					c.socket.async_write_some(boost::asio::buffer(c.send_buffer.data(),c.send_buffer.size()),std::bind(write_handler,async_handle(c,async_release),_1,_2));
				}

			} else {
				log("client %d (%s): write error: %s\n",c.client_id,c.address_string,ec.message());
				if (c.socket.is_open()) c.socket.close();
			}

		} catch (const std::exception&e) {
			log("client %d (%s): exception %s\n",c.client_id,c.address_string,e.what());
			if (c.socket.is_open()) c.socket.close();
		}

	};

	const size_t recv_size = 0x1000;
	const size_t max_send_buffer_size = 0x8000;
	const size_t max_recv_buffer_size = 0x8000;

	client_send_string = [&](client_t&c,const a_string&str) {

		size_t new_size = c.send_buffer.size()+str.size();
		if (new_size>max_send_buffer_size) xcept("send buffer is full (%u > %u)",new_size,max_send_buffer_size);

		size_t data_pos = c.send_buffer.size();
		c.send_buffer.resize(data_pos + str.size());
		memcpy(c.send_buffer.data()+data_pos,str.data(),str.size());

		c.last_send = ht.elapsed();

		timeout_set.erase(c);
		c.next_timeout = ht.elapsed() + timeout;
		timeout_set.insert(c);
		update_timer();

		if (!data_pos) {
			using namespace std::placeholders;
			c.socket.async_write_some(boost::asio::buffer(c.send_buffer.data(),c.send_buffer.size()),std::bind(write_handler,async_handle(c,async_release),_1,_2));
		}

	};

	std::function<void(client_t&c,const boost::system::error_code&,size_t)> read_handler = [&](client_t&c,const boost::system::error_code&ec,size_t bytes_transferred) {

		try {

			if (!ec) {

				c.recv_buffer.resize(c.recv_buffer.size() - recv_size + bytes_transferred);

				char*np = (char*)memchr(c.recv_buffer.data(),'\n',c.recv_buffer.size());
				if (np) {
					json_value jv;
					jv.parse(c.recv_buffer.data(),np-c.recv_buffer.data());
					on_client_message(c,jv);
					c.recv_buffer.erase(c.recv_buffer.begin(),c.recv_buffer.begin()+(np-c.recv_buffer.data())+1);
				}

				size_t new_size = c.recv_buffer.size() + recv_size;

				if (c.recv_buffer.size()>max_recv_buffer_size) {
					xcept("receive buffer is full (%u > %u)",new_size,max_recv_buffer_size);
				}

				c.recv_buffer.resize(new_size);

				using namespace std::placeholders;
				c.socket.async_read_some(boost::asio::buffer(c.recv_buffer.data()+c.recv_buffer.size()-recv_size,recv_size),std::bind(read_handler,async_handle(c,async_release),_1,_2));

			} else {
				log("client %d (%s): read error: %s\n",c.client_id,c.address_string,ec.message());
				if (c.socket.is_open()) c.socket.close();
			}

		} catch (const std::exception&e) {
			log("client %d (%s): exception %s\n",c.client_id,c.address_string,e.what());
			if (c.socket.is_open()) c.socket.close();
		}

	};

	boost::asio::io_service io_service;
	tcp::socket new_connection_socket(io_service);
	tcp::endpoint new_connection_endpoint;
	tcp::acceptor acceptor(io_service,tcp::endpoint(tcp::v4(), 8091));
	log("listening for connections on %s:%d\n",acceptor.local_endpoint().address().to_string(),acceptor.local_endpoint().port());

	int next_client_id = 0;

	std::function<void(const boost::system::error_code&)> new_connection_handler = [&](const boost::system::error_code&ec) {
		if (!ec) {
			log("%s connected\n",new_connection_endpoint.address().to_string());

			boost::asio::ip::tcp::no_delay option(true);
			new_connection_socket.set_option(option);

			clients.emplace_back(std::move(new_connection_socket));
			auto&c = clients.back();
			c.my_it = --clients.end();
			c.client_id = ++next_client_id;
			c.endpoint = new_connection_endpoint;
			c.address_string = c.endpoint.address().to_string().c_str();
			c.last_recv = ht.elapsed();
			
			c.next_timeout = ht.elapsed() + timeout;
			timeout_set.insert(c);

			c.recv_buffer.resize(recv_size);
			using namespace std::placeholders;
			c.socket.async_read_some(boost::asio::buffer(c.recv_buffer),std::bind(read_handler,async_handle(c,async_release),_1,_2));

			log("there are now %d servers and %d clients\n",servers.size(),clients.size());
		} else {
			log("accept error: %s\n",ec.message());
		}

		acceptor.async_accept(new_connection_socket,new_connection_endpoint,new_connection_handler);
	};

	acceptor.async_accept(new_connection_socket,new_connection_endpoint,new_connection_handler);

	boost::asio::high_resolution_timer timer(io_service,std::chrono::seconds(1));
	std::function<void(const boost::system::error_code&)> timer_handler = [&](const boost::system::error_code&ec) {
		
		if (!ec) {

			double now = ht.elapsed();

			if (!timeout_set.empty()) {
				auto&c = *timeout_set.begin();
				if (now>=c.next_timeout) {
					log("client %d (%s): timed out\n",c.client_id,c.endpoint.address().to_string());
					if (c.socket.is_open()) c.socket.close();

					timeout_set.erase(c);
					c.next_timeout = std::numeric_limits<double>::infinity();
					timeout_set.insert(c);
				}
			}

			update_timer();
		}

		timer.async_wait(timer_handler);
	};

	update_timer = [&]() {
		if (timeout_set.empty()) timer.expires_from_now(std::chrono::seconds(60));
		else {
			double t = timeout_set.begin()->next_timeout - ht.elapsed();
			if (t<0) t = 0;
			if (t>60) t = 60;

			// boost seems to have issues with duration<double>
			// milliseconds are fine for now
			std::chrono::milliseconds dur((int)(t*1000) + 1);
			timer.expires_from_now(dur);
		}
	};

	timer.async_wait(timer_handler);

	io_service.run();

}

int main(int argc, const char**argv) {
	try {

		current_log_level = log_level_all;

		//verify_test();

		net_server();

	} catch (const std::exception&e) {
		log(log_level_info," [exception] %s\n",e.what());
	}

	return 0;

}



CC      = g++
CFLAGS  = -O2 -g -std=c++11 -pthread
LDFLAGS = -lboost_system -pthread

BINARY  = hwo-server
SRCS    = main.cpp

all: $(BINARY)

$(BINARY): $(SRCS:%.cpp=%.o)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $(BINARY) $(SRCS:%.cpp=%.o)

.cpp.o:
	$(CC) $(CFLAGS) -c -MD -o $@ $<

clean:
	-rm -f $(SRCS:%.cpp=%.o) $(SRCS:%.cpp=%.d) $(BINARY)

.PHONY: clean

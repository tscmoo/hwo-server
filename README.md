## tscmoo's HWO2014 server


This is my implementation of the Hello World Open 2014 server.

Only the joinRace message is supported (not join or createRace).


Tracks are loaded from tracks/<trackname>.json.
Recordings are stored to recordings/<uid>.json.

If you want to watch a recording, then currently you need to host it on a web-server and use the official visualizer.


Errors are currently never sent to the clients, so if something goes wrong (eg. invalid throttle value), then the client is simply disconnected and one has to check the server output to see why.

The physics and race parameters are hard-coded, to change them look around line 140 in server.h.



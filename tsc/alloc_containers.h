

namespace tsc {
;


template<typename T>
struct a_vector : std::vector<T,alloc<T>> {
	typedef std::vector<T,alloc<T>> type;
	a_vector() : type() {}
	a_vector(size_t size) : type(size) {}
};


template<typename T>
struct a_deque: std::deque<T,alloc<T>> {};
template<typename T>
struct a_list: std::list<T,alloc<T>> {};

template<typename T>
struct a_set: std::set<T,std::less<T>,alloc<T>> {};
template<typename T>
struct a_multiset: std::multiset<T,std::less<T>,alloc<T>> {};

template<typename key_T,typename value_T,typename pred_T = std::less<key_T>>
struct a_map: std::map<key_T,value_T,pred_T,alloc<std::pair<key_T,value_T>>> {};
template<typename key_T,typename value_T,typename pred_T = std::less<key_T>>
struct a_multimap: std::multimap<key_T,value_T,pred_T,alloc<std::pair<key_T,value_T>>> {};

template<typename T,typename hash_t = std::hash<T>,typename equal_to_t = std::equal_to<T>>
struct a_unordered_set: std::unordered_set<T,hash_t,equal_to_t,alloc<T>>  {};
template<typename T,typename hash_t = std::hash<T>,typename equal_to_t = std::equal_to<T>>
struct a_unordered_multiset: std::unordered_multiset<T,hash_t,equal_to_t,alloc<T>>  {};

template<typename key_T,typename value_T,typename hash_t = std::hash<key_T>,typename equal_to_t = std::equal_to<key_T>>
struct a_unordered_map: std::unordered_map<key_T,value_T,hash_t,equal_to_t,alloc<std::pair<key_T,value_T>>> {};
template<typename key_T,typename value_T,typename hash_t = std::hash<key_T>,typename equal_to_t = std::equal_to<key_T>>
struct a_unordered_multimap: std::unordered_multimap<key_T,value_T,hash_t,equal_to_t,alloc<std::pair<key_T,value_T>>> {};


typedef std::basic_string<char,std::char_traits<char>,alloc<char>> a_string;

//template<typename T>
//struct a_circular_queue: circular_queue<T,alloc<T>> {};

}


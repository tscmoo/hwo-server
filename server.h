
namespace hwo {
;

struct car {
	a_string color, name;
	double guide_flag_pos;
	double length, width;
};

struct track_t {
	a_string id, name;
	a_vector<double> lanes;
	struct piece {
		double length;
		double angle, radius;
		bool is_switch;

		double track_pos;

		double abs_x, abs_y, abs_angle;
	};
	a_vector<piece> pieces;
};

struct race {
	a_vector<car> cars;
	track_t track;
	double laps;
	double max_lap_time_ms;
	bool quick_race;
};

struct state {
	struct car_state {
		int piece_index;
		double piece_pos;
		size_t start_lane, end_lane;
		double speed;
		double angle;

		double throttle;
		double rotspeed;
		int next_switch;

		int lap;
		int crashed_until;

		int last_passed_start;
		int last_lap_time;
		
		bool has_turbo;
		bool use_turbo;
		int turbo_until;

		bool dnf, please_dnf;
		const char*dnf_reason;

		int best_lap_n;
		int best_lap_time;
		int finished_time;

		double abs_x, abs_y, abs_angle;
		double abs_velocity_x, abs_velocity_y;
		double last_track_angle;
		double crash_angle;
	};

	int tick;
	a_vector<car_state> cars;
	int last_turbo_available;
	int finished_count;
};

struct options {
	double acceleration;
	double resistance;

	double angle_acceleration_factor;
	double angle_resistance_factor;
	double slip_factor;
	double slip_weight_factor;

	int laps;
	int max_lap_ticks;
	bool quick_race;
	
	int turbo_interval;
	int turbo_ticks;
	double turbo_factor;
};

static const double base_angle_acceleration = -0.00125;
static const double base_angle_resistance = -0.1;

static const double base_slip = sqrt(9.0/32)/180*PI;
static const double base_slip_weight = -0.3/180*PI;

struct server {

	options opt;

	json_value track_json;
	track_t track;
	a_vector<car> cars;
	a_string game_id;

	state st;

	struct switch_piece_t {
		double radius;
		double angle;
		double pos_angle;
		double pos_radius;
	};

	a_vector<double> lane_lengths;
	a_vector<std::array<switch_piece_t,100>*> lane_switch_pieces;
	a_deque<std::array<switch_piece_t,100>> lane_switch_piece_data;
	a_vector<double> abs_lane_pos;
	a_vector<std::tuple<double,size_t>> live_ranking;

	int respawn_time;
	double crash_angle;

	double angle_acceleration_value;
	double angle_resistance_value;
	double slip_value;
	double slip_weight_value;

	server() {
		opt.acceleration = 0.2;
		opt.resistance = -0.02;

		opt.angle_acceleration_factor = 1.0;
		opt.angle_resistance_factor = 1.0;
		opt.slip_factor = 1.0;
		opt.slip_weight_factor = 1.0;

		opt.laps = 10;
		opt.max_lap_ticks = 3600;
		opt.quick_race = true;
		
		opt.turbo_interval = 600;
		opt.turbo_ticks = 30;
		opt.turbo_factor = 3.0;

		set_options(opt);

		respawn_time = 400;
		crash_angle = 60.0/180*PI;
	}
	void set_options(options opt) {
		this->opt = opt;

		angle_acceleration_value = base_angle_acceleration * opt.angle_acceleration_factor;
		angle_resistance_value = base_angle_resistance * opt.angle_resistance_factor;
		slip_value = base_slip * opt.slip_factor;
		slip_weight_value = base_slip_weight * opt.slip_weight_factor;
	}

	double radius_from_three_points(double x1,double y1,double x2,double y2,double x3,double y3) {
		double x21 = x2-x1;
		double y21 = y2-y1;
		double x23 = x2-x3;
		double y23 = y2-y3;
		double x31 = x3-x1;
		double y31 = y3-y1;
		double u = sqrt((x21*x21 + y21*y21) * (x23*x23 + y23*y23) * (x31*x31 + y31*y31));
		double l = 2 * std::abs(x1*y2 + x2*y3 + x3*y1 - x1*y3 - x2*y1 - x3*y2);
		return u / l;
	}

	double cubic_bezier(double t,double a,double b,double c,double d) {
		double t2 = t * t;
		double t3 = t2 * t;
		double mt = (1-t);
		double mt2 = mt*mt;
		double mt3 = mt*mt*mt;
		return mt3*a + 3*mt2*t*b + 3*mt*t2*c + t3*d;
	};

	double quadratic_bezier(double t,double a,double b,double c) {
		double t2 = t * t;
		double mt = (1-t);
		double mt2 = mt*mt;
		return mt2*a + 2*mt*t*b + t2*c;
	};

	template<typename cb_F>
	void walk_curved_switch(double angle,double from_radius,double to_radius,cb_F&&cb) {

		double start_x = 0.0;
		double start_y = 0.0;

		double end_x = 1.0*from_radius - cos(angle)*to_radius;
		double end_y = 0.0*from_radius - sin(angle)*to_radius;

		double mid_x = 2*(1.0*from_radius - cos(angle/2)*(from_radius+to_radius)/2) - start_x/2 - end_x/2;
		double mid_y = 2*(0.0*from_radius - sin(angle/2)*(from_radius+to_radius)/2) - start_y/2 - end_y/2;

		double step = 1.0/10000;
		for (double t=step;t<1.0+step;t+=step) {

			double x = -quadratic_bezier(t,start_x,mid_x,end_x);
			double y = -quadratic_bezier(t,start_y,mid_y,end_y);

			cb(x,y);

		}

	}

	template<typename cb_F>
	void walk_straight_switch(double end_x,double end_y,cb_F&&cb) {

		double start_x = 0.0;
		double start_y = 0.0;

		double c1x = end_x*0.1;
		double c1y = end_y*0.25;
		double c2x = end_x*0.875;
		double c2y = end_y*0.75;

		double step = 1.0/10000;
		for (double t=step;t<1.0+step;t+=step) {

			double x = cubic_bezier(t,0.0,c1x,c2x,end_x);
			double y = cubic_bezier(t,0.0,c1y,c2y,end_y);

			cb(x,y);

		}

	}

	template<typename cb_F>
	void walk_switch(double length,double angle,double lane1,double lane2,cb_F&&cb) {
		if (angle) walk_curved_switch(angle,lane1,lane2,cb);
		else walk_straight_switch(lane2-lane1,length,cb);
	}


	void calculate_track_stuff() {

		a_map<std::tuple<double,double,double,double>,std::tuple<double,std::array<switch_piece_t,100>*>> cache;

		lane_switch_piece_data.clear();
		lane_lengths.clear();
		lane_switch_pieces.clear();
		lane_lengths.resize(track.pieces.size()*track.lanes.size()*3);
		lane_switch_pieces.resize(track.pieces.size()*track.lanes.size()*2);
		for (size_t pidx=0;pidx<track.pieces.size();++pidx) {
			auto&p = track.pieces[pidx];

			//log("piece %d - %f %f %f %d\n",pidx,p.length,p.angle,p.radius,p.is_switch);
			
			for (size_t l=0;l<track.lanes.size();++l) {
				double*ptr = &lane_lengths[pidx*track.lanes.size()*3 + l*3];
				double&swl = ptr[0];
				double&nosw = ptr[1];
				double&swr = ptr[2];

				if (p.radius) {
					double lanepos = track.lanes[l];
					double radius = p.radius + (p.angle<0 ? lanepos : -lanepos);
					nosw = std::abs(radius * p.angle);
				} else {
					nosw = p.length;
				}

				if (!p.is_switch) {
					swl = 0;
					swr = 0;
				} else {
					double lanepos1 = track.lanes[l];
					double radius_or_lanepos_1 = p.radius + (p.angle<0 ? lanepos1 : -lanepos1);
					auto set = [&](int sw) {
						double lanepos2 = track.lanes[l+sw];
						double radius_or_lanepos_2 = p.radius + (p.angle<0 ? lanepos2 : -lanepos2);

						auto i = cache.emplace(std::piecewise_construct,std::make_tuple(p.length,p.angle,radius_or_lanepos_1,radius_or_lanepos_2),std::make_tuple());
						auto&length = std::get<0>(i.first->second);
						auto*&piece_data_ptr = std::get<1>(i.first->second);
						if (i.second) {
							lane_switch_piece_data.emplace_back();
							piece_data_ptr = &lane_switch_piece_data.back();
							auto&piece_data = *piece_data_ptr;
							double lx = 0.0, ly = 0.0;
							walk_switch(p.length,p.angle,radius_or_lanepos_1,radius_or_lanepos_2,[&](double x,double y) {
								length += hypot(x-lx,y-ly);
								lx = x;
								ly = y;
							});

							//log("pidx %d - length for %f %f %f %f is %f\n",pidx,p.length,p.angle/PI*180,radius_or_lanepos_1,radius_or_lanepos_2,length);

							lx = 0; ly = 0;
							double llx = 0, lly = 0;
							double lpx = 0, lpy = 0;
							double llpx = 0, llpy = 0;
							double piece_size = length / 100.0;
							size_t idx = 0;
							double accumulated_length = 0.0;
							auto add = [&](double llpx,double llpy,double lpx,double lpy,double x,double y) {
								double r = radius_from_three_points(llpx,llpy,lpx,lpy,x,y);
								//log("%d - radius %f\n",idx,r);
								piece_data[idx].radius = r;
								piece_data[idx].pos_angle = atan2(lpy,lpx) - PI/2 + (p.angle<0 ? PI : 0);
								piece_data[idx].pos_radius = hypot(lpx,lpy);
								if (idx==0) piece_data[idx].angle = atan2(y,x) - PI/2 + (p.angle<0 ? PI : 0);
								else piece_data[idx].angle = atan2(lpy-llpy,lpx-llpx) - PI/2 + (p.angle<0 ? PI : 0);
							};
							walk_switch(p.length,p.angle,radius_or_lanepos_1,radius_or_lanepos_2,[&](double x,double y) {
								double l = hypot(x-lx,y-ly);
								accumulated_length += l;
								if (accumulated_length>=piece_size && idx<99) {
									accumulated_length -= piece_size;

									add(llpx,llpy,lpx,lpy,x,y);

									++idx;

									llpx = lpx;
									llpy = lpy;
									lpx = x;
									lpy = y;
								}
								llx = lx;
								lly = ly;
								lx = x;
								ly = y;
							});
							add(llpx,llpy,lpx,lpy,llx,lly);
						}
						lane_switch_pieces[pidx*track.lanes.size()*2 + l*2 + (sw==-1 ? 0 : 1)] = piece_data_ptr;

						if (sw==-1) swl = length;
						if (sw==1) swr = length;
					};
					if (l!=0) {
						set(-1);
					} else swl = 0;
					if (l!=track.lanes.size()-1) {
						set(1);
					} else swr = 0;
					
				}

				//log("piece %d - %f %f %f\n",pidx,swl,nosw,swr);
			}

		}

		abs_lane_pos.resize(track.pieces.size() * track.lanes.size());
		for (size_t l=0;l<track.lanes.size();++l) {
			double pos = 0.0;
			for (size_t i=0;i<track.pieces.size();++i) {
				abs_lane_pos[i*track.lanes.size() + l] = pos;
				pos += get_lane_length(i,l,l);
			}
		}

	}

	void set_track(const json_value&track_data_arg) {

		auto track_data = track_data_arg;
		track.lanes.clear();
		track.pieces.clear();
		track_json = json_value();

		track.id = (a_string)track_data["id"];
		track.name = (a_string)track_data["name"];
		for (auto&v : track_data["lanes"].vector) {
			size_t index = (int)v["index"];
			if (index>=0x100) xcept("invalid lane index %d",index);
			if (track.lanes.size()<=index) track.lanes.resize(index+1);
			track.lanes[index] = v["distanceFromCenter"];
		}

		auto&starting_point = track_data["startingPoint"];
		double start_angle = (double)starting_point["angle"]/180.0*PI;
		auto&starting_pos = starting_point["position"];
		double start_x = starting_pos["x"];
		double start_y = starting_pos["y"];

		double abs_angle = start_angle;
		double abs_x = start_x*cos(start_angle) - start_y*sin(start_angle);
		double abs_y = -start_x*cos(start_angle + PI/2) + start_y*sin(start_angle + PI/2);

		double pos = 0;

		for (auto&v : track_data["pieces"].vector) {
			if (track.pieces.size()>=0x10000) xcept("too many track pieces");
			track.pieces.emplace_back();
			auto&p = track.pieces.back();
			p.length = v["length"];
			p.angle = (double)v["angle"]/180*PI;
			p.radius = v["radius"];
			p.is_switch = v["switch"];

			p.abs_x = abs_x;
			p.abs_y = abs_y;
			p.abs_angle = abs_angle;

			p.track_pos = pos;

			if (p.radius) {
				double length = std::abs(p.angle * p.radius);
				pos += length;

				double tx = p.abs_x + cos(abs_angle+(p.angle<0 ? PI : 0))*p.radius;
				double ty = p.abs_y + sin(abs_angle+(p.angle<0 ? PI : 0))*p.radius;

				abs_angle += p.angle;

				abs_x = tx + cos(abs_angle+(p.angle<0 ? 0 : PI))*p.radius;
				abs_y = ty + sin(abs_angle+(p.angle<0 ? 0 : PI))*p.radius;
				
			} else {
				pos += p.length;

				abs_x += cos(abs_angle - PI/2)*p.length;
				abs_y += sin(abs_angle - PI/2)*p.length;
			}

		}
		track_json = track_data_arg;

		calculate_track_stuff();

		if (track.lanes.empty() || track.pieces.empty()) xcept("invalid track");

	}


	size_t add_car(a_string name,a_string color) {

		if (cars.size()>=0x100) xcept("too many cars");

		for (auto&v : cars) {
			if (v.name==name) xcept("name already taken");
			if (v.color==color) xcept("color already taken");
		}

		car c;
		c.name = name;
		c.color = color;
		c.guide_flag_pos = 10;
		c.length = 40;
		c.width = 20;

		cars.push_back(c);
		return cars.size()-1;
	}
	
	json_value make_gameInit() {
		json_value jv;
		jv["msgType"] = "gameInit";
		jv["gameId"] = game_id;
		auto&data = jv["data"];
		auto&jv_race= data["race"];
		jv_race["track"] = track_json;
		auto&jv_cars = jv_race["cars"];
		log("there are %d cars\n",cars.size());
		for (size_t i=0;i<cars.size();++i) {
			auto&c = cars[i];
			auto&jv_c = jv_cars[i];
			auto&jv_id = jv_c["id"];
			log("car %d name is %s\n",i,c.name);
			jv_id["name"] = c.name;
			jv_id["color"] = c.color;
			auto&jv_dim = jv_c["dimensions"];
			jv_dim["guideFlagPosition"] = c.guide_flag_pos;
			jv_dim["length"] = c.length;
			jv_dim["width"] = c.width;
		}
		auto&jv_session = jv_race["raceSession"];
		jv_session["laps"] = opt.laps;
		jv_session["maxLapTimeMs"] = (int)(opt.max_lap_ticks*(1000.0/60));
		jv_session["quickRace"] = opt.quick_race;
		return jv;
	}

	json_value make_carPositions() {
		json_value jv;
		jv["msgType"] = "carPositions";
		jv["gameId"] = game_id;
		if (st.tick!=-1 && st.finished_count!=st.cars.size()) jv["gameTick"] = st.tick;
		auto&data = jv["data"];
		data.type = json_value::t_array;
		data.vector.resize(st.cars.size());
		for (size_t i=0;i<st.cars.size();++i) {
			auto&st_c = st.cars[i];
			auto&jv_c = data[i];
			jv_c["id"]["name"] = cars[i].name;
			jv_c["id"]["color"] = cars[i].color;
			jv_c["angle"] = st_c.angle*180/PI;
			if (st.tick!=-1) jv_c["prevCommandTick"] = st.tick-1;
			auto&pos = jv_c["piecePosition"];
			pos["pieceIndex"] = st_c.piece_index;
			pos["inPieceDistance"] = st_c.piece_pos;
			auto&lane = pos["lane"];
			lane["startLaneIndex"] = (int)st_c.start_lane;
			lane["endLaneIndex"] = (int)st_c.end_lane;
			pos["lap"] = st_c.lap;
		}

		return jv;
	}

	json_value make_gameStart() {
		json_value jv;
		jv["msgType"] = "gameStart";
		jv["gameId"] = game_id;
		jv["gameTick"] = st.tick;
		jv["data"] = json_value();
		return jv;
	}

	json_value make_lapFinished(size_t car_index) {
		json_value jv;
		jv["msgType"] = "lapFinished";
		jv["gameId"] = game_id;
		if (st.tick!=-1 && st.finished_count!=st.cars.size()) jv["gameTick"] = st.tick;
		auto&data = jv["data"];
		auto&jv_car = data["car"];
		auto&c = cars[car_index];
		jv_car["name"] = cars[car_index].name;
		jv_car["color"] = cars[car_index].color;
		auto&jv_lt = data["lapTime"];
		jv_lt["lap"] = st.cars[car_index].lap-1;
		jv_lt["ticks"] = st.cars[car_index].last_lap_time;
		// How is this rounded?
		// 416 is 6933 and 476 is 7934. Explain that.
		jv_lt["millis"] = (int)(st.cars[car_index].last_lap_time*(1000.0/60) + 0.5);
		auto&jv_rt = data["raceTime"];
		jv_rt["laps"] = st.cars[car_index].lap;
		jv_rt["ticks"] = st.tick;
		jv_rt["millis"] = (int)(st.tick*(1000.0/60) + 0.5);
		auto&jv_ranking = data["ranking"];
		int fastest_lap_rank = 1;
		for (auto&v : st.cars) {
			if (v.best_lap_time && v.best_lap_time<st.cars[car_index].best_lap_time) ++fastest_lap_rank;
		}
		jv_ranking["fastestLap"] = fastest_lap_rank;
		int overall_rank = 1;
		for (auto&v : live_ranking) {
			if (std::get<1>(v)==car_index) break;
			overall_rank++;
		}
		jv_ranking["overall"] = overall_rank;
		return jv;
	}

	json_value make_turboAvailable() {
		json_value jv;
		jv["msgType"] = "turboAvailable";
		jv["gameId"] = game_id;
		jv["gameTick"] = st.tick;
		auto&data = jv["data"];
		data["turboDurationTicks"] = opt.turbo_ticks;
		data["turboDurationMilliseconds"] = opt.turbo_ticks*(1000.0/60);
		data["turboFactor"] = opt.turbo_factor;
		return jv;
	}

	json_value make_turboStart(size_t car_index) {
		json_value jv;
		jv["msgType"] = "turboStart";
		jv["gameId"] = game_id;
		jv["gameTick"] = st.tick;
		auto&data = jv["data"];
		data["name"] = cars[car_index].name;
		data["color"] = cars[car_index].color;
		return jv;
	}

	json_value make_turboEnd(size_t car_index) {
		json_value jv;
		jv["msgType"] = "turboEnd";
		jv["gameId"] = game_id;
		jv["gameTick"] = st.tick;
		auto&data = jv["data"];
		data["name"] = cars[car_index].name;
		data["color"] = cars[car_index].color;
		return jv;
	}

	json_value make_finish(size_t car_index) {
		json_value jv;
		jv["msgType"] = "finish";
		jv["gameId"] = game_id;
		auto&data = jv["data"];
		data["name"] = cars[car_index].name;
		data["color"] = cars[car_index].color;
		return jv;
	}

	json_value make_gameEnd() {
		json_value jv;
		jv["msgType"] = "gameEnd";
		jv["gameId"] = game_id;
		auto&data = jv["data"];
		auto&best_laps = data["bestLaps"];
		best_laps.type = json_value::t_array;
		best_laps.vector.resize(st.cars.size());
		for (size_t i=0;i<st.cars.size();++i) {
			if (!st.cars[i].best_lap_n) continue;
			auto&jv_v = best_laps[i];
			auto&jv_c = jv_v["car"];
			jv_c["name"] = cars[i].name;
			jv_c["color"] = cars[i].color;
			auto&jv_r = jv_v["result"];
			jv_r["lap"] = st.cars[i].best_lap_n;
			jv_r["ticks"] = st.cars[i].best_lap_time;
			jv_r["millis"] = (int)(std::ceil(st.cars[i].best_lap_time*(1000.0/60)));
		}
		auto&results = data["results"];
		best_laps.type = json_value::t_array;
		best_laps.vector.resize(st.cars.size());
		for (size_t i=0;i<st.cars.size();++i) {
			if (!st.cars[i].finished_time) continue;
			auto&jv_v = results[i];
			auto&jv_c = jv_v["car"];
			jv_c["name"] = cars[i].name;
			jv_c["color"] = cars[i].color;
			auto&jv_r = jv_v["result"];
			jv_r["laps"] = st.cars[i].lap;
			jv_r["ticks"] = st.cars[i].finished_time;
			jv_r["millis"] = (int)(std::ceil(st.cars[i].finished_time*(1000.0/60)));
		}
		return jv;
	}

	json_value make_tournamentEnd() {
		json_value jv;
		jv["msgType"] = "tournamentEnd";
		jv["gameId"] = game_id;	
		jv["data"] = json_value();
		return jv;
	}

	json_value make_crash(size_t car_index) {
		json_value jv;
		jv["msgType"] = "crash";
		jv["gameId"] = game_id;
		jv["gameTick"] = st.tick;
		auto&data = jv["data"];
		data["name"] = cars[car_index].name;
		data["color"] = cars[car_index].color;
		return jv;
	}

	json_value make_spawn(size_t car_index) {
		json_value jv;
		jv["msgType"] = "spawn";
		jv["gameId"] = game_id;
		jv["gameTick"] = st.tick;
		auto&data = jv["data"];
		data["name"] = cars[car_index].name;
		data["color"] = cars[car_index].color;
		return jv;
	}

	json_value make_gameStartsIn(double v) {
		json_value jv;
		jv["msgType"] = "gameStartsIn";
		jv["gameId"] = game_id;
		jv["data"] = v;
		return jv;
	}

	json_value make_fullCarPositions() {
		json_value jv;
		jv["msgType"] = "fullCarPositions";
		jv["gameId"] = game_id;
		if (st.tick!=-1 && st.finished_count!=st.cars.size()) jv["gameTick"] = st.tick;
		auto&data = jv["data"];
		data.type = json_value::t_array;
		data.vector.resize(st.cars.size());
		for (size_t i=0;i<st.cars.size();++i) {
			auto&st_c = st.cars[i];
			auto&jv_c = data[i];
			jv_c["id"]["name"] = cars[i].name;
			jv_c["id"]["color"] = cars[i].color;
			
			auto&coords = jv_c["coordinates"];
			double lanepos = track.lanes[st_c.start_lane];
			coords["x"] = st_c.abs_x;
			coords["y"] = st_c.abs_y;
			jv_c["angle"] = st_c.abs_angle/PI*180.0;

			jv_c["angleOffset"] = st_c.angle*180/PI;
			auto&pos = jv_c["piecePosition"];
			pos["pieceIndex"] = st_c.piece_index;
			pos["inPieceDistance"] = st_c.piece_pos;
			auto&lane = pos["lane"];
			lane["startLaneIndex"] = (int)st_c.start_lane;
			lane["endLaneIndex"] = (int)st_c.end_lane;
			pos["lap"] = st_c.lap;
		}
		return jv;
	}
	json_value make_carVelocities() {
		json_value jv;
		jv["msgType"] = "carVelocities";
		jv["gameId"] = game_id;
		if (st.tick!=-1 && st.finished_count!=st.cars.size()) jv["gameTick"] = st.tick;
		auto&data = jv["data"];
		data.type = json_value::t_array;
		data.vector.resize(st.cars.size());
		for (size_t i=0;i<st.cars.size();++i) {
			auto&st_c = st.cars[i];
			auto&jv_c = data[i];
			jv_c["x"] = st_c.abs_velocity_x;
			jv_c["y"] = st_c.abs_velocity_y;
		}
		return jv;
	}

	json_value make_liveRanking() {
		json_value jv;
		jv["msgType"] = "liveRanking";
		jv["gameId"] = game_id;
		jv["gameTick"] = st.tick;
		auto&data = jv["data"];
		data.type = json_value::t_array;
		data.vector.resize(st.cars.size());
		for (size_t i=0;i<live_ranking.size();++i) {
			auto&jv_n = data[i];
			auto&c = cars[std::get<1>(live_ranking[i])];
			jv_n["name"] = c.name;
			jv_n["color"] = c.color;
		}
		return jv;
	}

	json_value make_bump(size_t bumpee,size_t bumper) {
		json_value jv;
		jv["msgType"] = "bump";
		jv["gameId"] = game_id;
		jv["gameTick"] = st.tick;
		auto&data = jv["data"];
		auto&jv_bumpee = data["bumpee"];
		jv_bumpee["name"] = cars[bumpee].name;
		jv_bumpee["color"] = cars[bumpee].color;
		auto&jv_bumper = data["bumper"];
		jv_bumper["name"] = cars[bumper].name;
		jv_bumper["color"] = cars[bumper].color;
		return jv;
	}
	json_value make_dnf(size_t car_index) {
		json_value jv;
		jv["msgType"] = "bump";
		jv["gameId"] = game_id;
		jv["gameTick"] = st.tick + 1;
		auto&data = jv["data"];
		auto&car = data["car"];
		car["name"] = cars[car_index].name;
		car["color"] = cars[car_index].color;
		data["reason"] = st.cars[car_index].dnf_reason ? st.cars[car_index].dnf_reason : "unknown";
		return jv;
	}

	bool update_live_ranking() {
		if (live_ranking.empty()) {
			for (size_t i=0;i<cars.size();++i) {
				live_ranking.emplace_back(0.0,i);
			}
		}
		for (auto&v : live_ranking) {
			auto&c = st.cars[std::get<1>(v)];
			auto&p = track.pieces[c.piece_index];
			std::get<0>(v) = -(int)track.pieces.size()*c.lap - c.piece_index - c.piece_pos/get_lane_length(c.piece_index,c.start_lane,c.end_lane);
			if (c.finished_time) {
				std::get<0>(v) = -(int)track.pieces.size()*c.lap - 1000.0/c.finished_time;
			}
		}
		if (std::is_sorted(live_ranking.begin(),live_ranking.end())) return false;
		std::sort(live_ranking.begin(),live_ranking.end());
		return true;
	}

	void update_abs_values(state::car_state&c) {
		auto&p = track.pieces[c.piece_index];

		if (c.crashed_until) {
			c.abs_angle += c.rotspeed;
			c.abs_x += cos(c.crash_angle) * c.speed;
			c.abs_y += sin(c.crash_angle) * c.speed;
			return;
		}

		if (c.start_lane!=c.end_lane) {
			double length = get_lane_length(c.piece_index,c.start_lane,c.end_lane);
			size_t idx = (size_t)(c.piece_pos / length * 100.0);
			if (idx>=100) xcept("bad switch radius index %d",idx);

			auto*sw_piece_ptr = lane_switch_pieces[c.piece_index*track.lanes.size()*2+c.start_lane*2+(c.end_lane>c.start_lane ? 1 : 0)];
			if (!sw_piece_ptr) xcept("null switch piece");

			auto&sw_piece = (*sw_piece_ptr)[idx];
			
			c.abs_angle = p.abs_angle + sw_piece.angle + c.angle;
			c.last_track_angle = p.abs_angle + sw_piece.angle;

			double lanepos = track.lanes[c.start_lane];
			c.abs_x = p.abs_x + cos(p.abs_angle)*lanepos;
			c.abs_y = p.abs_y + sin(p.abs_angle)*lanepos;

			c.abs_x += cos(sw_piece.pos_angle + p.abs_angle - PI/2)*sw_piece.pos_radius;
			c.abs_y += sin(sw_piece.pos_angle + p.abs_angle - PI/2)*sw_piece.pos_radius;

		} else {
			if (p.radius) {

				double pos = c.piece_pos / get_lane_length(c.piece_index,c.start_lane,c.end_lane);
				c.abs_angle = p.abs_angle + p.angle*pos;
				c.last_track_angle = c.abs_angle;

				double tx = p.abs_x + cos(p.abs_angle+(p.angle<0 ? PI : 0))*p.radius;
				double ty = p.abs_y + sin(p.abs_angle+(p.angle<0 ? PI : 0))*p.radius;

				double lanepos = track.lanes[c.start_lane];
				c.abs_x = tx + cos(c.abs_angle+(p.angle<0 ? 0 : PI))*(p.radius + (p.angle<0 ? lanepos : -lanepos));
				c.abs_y = ty + sin(c.abs_angle+(p.angle<0 ? 0 : PI))*(p.radius + (p.angle<0 ? lanepos : -lanepos));

				c.abs_angle += c.angle;

			} else {
				double lanepos = track.lanes[c.start_lane];
				c.abs_x = p.abs_x + cos(p.abs_angle)*lanepos + cos(p.abs_angle - PI/2)*c.piece_pos;
				c.abs_y = p.abs_y + sin(p.abs_angle)*lanepos + sin(p.abs_angle - PI/2)*c.piece_pos;

				c.abs_angle = p.abs_angle + c.angle;
				c.last_track_angle = p.abs_angle;
			}
		}

		c.abs_velocity_x = 0;
		c.abs_velocity_y = 0;
	}

	template<typename server_msg_F,typename replay_msg_F>
	void start(server_msg_F&&server_msg,replay_msg_F&&replay_msg) {

		for (size_t i=0;i<cars.size();++i) {
			auto&v = cars[i];
			json_value jv;
			jv["msgType"] = "yourCar";
			jv["gameId"] = game_id;
			auto&data = jv["data"];
			data["color"] = v.color;
			data["name"] = v.name;
			
			server_msg(i,jv);
		}

		auto msg_gameInit = make_gameInit();
		server_msg(-1,msg_gameInit);
		replay_msg(msg_gameInit);

		st.tick = -1;
		st.cars.clear();
		st.cars.resize(cars.size());
		for (size_t i=0;i<cars.size();++i) {
			auto&c = cars[i];
			auto&st_c = st.cars[i];
			st_c.piece_index = 0;
			st_c.piece_pos = -25.0*i;
			st_c.start_lane = i%track.lanes.size();
			st_c.end_lane = i%track.lanes.size();
			int lap = 0;
			while (st_c.piece_pos<0) {
				if (st_c.piece_index==0) {
					st_c.piece_index = track.pieces.size()-1;
					--lap;
				} else --st_c.piece_index;
				st_c.piece_pos += get_lane_length(st_c.piece_index,st_c.start_lane,st_c.start_lane);
			}
			st_c.speed = 0;
			st_c.angle = 0;

			st_c.throttle = 0;
			st_c.rotspeed = 0;
			st_c.next_switch = 0;

			st_c.lap = lap;
			st_c.crashed_until = 0;

			st_c.last_passed_start = 0;
			st_c.last_lap_time = 0;

			st_c.has_turbo = false;
			st_c.use_turbo = false;
			st_c.turbo_until = 0;

			st_c.dnf = st_c.please_dnf = false;
			st_c.dnf_reason = nullptr;

			st_c.best_lap_n = 0;
			st_c.best_lap_time = 0;
			st_c.finished_time = 0;

			st_c.abs_x = st_c.abs_y = st_c.abs_angle = 0;
			st_c.abs_velocity_x = st_c.abs_velocity_y = 0;
			st_c.last_track_angle = 0;
			st_c.crash_angle = 0;

			update_abs_values(st_c);
		}

		replay_msg(make_gameStartsIn(0.0));

		server_msg(-1,make_carPositions());
		replay_msg(make_fullCarPositions());
		replay_msg(make_carVelocities());
		st.tick = 0;
		auto msg_gameStart = make_gameStart();
		server_msg(-1,msg_gameStart);
		replay_msg(msg_gameStart);

		st.last_turbo_available = 1;
		st.finished_count = 0;

	}

	void client_dnf(size_t client_id) {
		if (client_id>=st.cars.size()) xcept("invalid client id");
		auto&st_c = st.cars[client_id];
		st_c.please_dnf = true;
	}

	void client_message(size_t client_id,json_value&jv) {
		if (client_id>=st.cars.size()) xcept("invalid client id");
		auto&st_c = st.cars[client_id];

		if (st_c.dnf) xcept("dnf");

		auto&msg_type = jv["msgType"];
		auto&tick = jv["gameTick"];
		if (tick && (int)tick!=st.tick) xcept("tick mismatch");
		if (msg_type=="throttle") {
			auto&data = jv["data"];
			if (data.type!=json_value::t_number || data.n<0 || data.n>1) xcept("invalid throttle value");
			st_c.throttle = data.n;
		} else if (msg_type=="switchLane") {
			auto&data = jv["data"];
			if (data=="Right") st_c.next_switch = 1;
			else if (data=="Left") st_c.next_switch = -1;
			else xcept("bad switch message");
			size_t l = st_c.end_lane + st_c.next_switch;
			if (l>=track.lanes.size()) {
				st_c.next_switch = 0;
				xcept("bad switch");
			}
		} else if (msg_type=="turbo") {
			const a_string&turbo_message = jv["data"];
			st.cars[client_id].use_turbo = true;
		} else if (msg_type=="ping") {

		} else xcept("unknown message type %s\n",(a_string)msg_type);

	};

	double acceleration(double throttle,double speed) {
		return opt.acceleration*throttle + opt.resistance*speed;
	}

	double angle_correction(double speed,double angle,double rot) {
		return angle_acceleration_value*speed*angle + angle_resistance_value*rot;
	}

	double angle_slip(double speed,double radius) {
		return (slip_value/sqrt(radius)*speed + slip_weight_value)*speed;
	}

	double get_lane_length(size_t piece_index,size_t start_lane,size_t end_lane) {
		int sw = 1;
		if (end_lane<start_lane) sw = 0;
		if (end_lane>start_lane) sw = 2;
		return lane_lengths[piece_index*track.lanes.size()*3+start_lane*3+sw];
	}

	double get_abs_lane_pos(size_t piece_index,size_t lane) {
		return abs_lane_pos[piece_index*track.lanes.size() + lane];
	}

	template<typename server_msg_F,typename replay_msg_F>
	bool run(server_msg_F&&server_msg,replay_msg_F&&replay_msg) {

		auto server_and_replay_msg = [&](const json_value&jv) {
			server_msg(-1,jv);
			replay_msg(jv);
		};

		bool someone_passed_start = false;
		auto check_piece_transition = [&](size_t car_index) {
			auto&c = st.cars[car_index];
			double l;
			while (c.piece_pos>(l=get_lane_length(c.piece_index,c.start_lane,c.end_lane))) {
				c.piece_pos -= l;
				++c.piece_index;
				if (c.piece_index==track.pieces.size()) {
					c.piece_index = 0;
					if (c.lap>=0) {
						c.last_lap_time = st.tick - c.last_passed_start;

						if (c.last_lap_time<c.best_lap_time || !c.best_lap_time) {
							c.best_lap_n = c.lap;
							c.best_lap_time = c.last_lap_time;
						}
					}
					c.last_passed_start = st.tick;

					++c.lap;
					someone_passed_start = true;

					if (c.lap==opt.laps) {
						++st.finished_count;
						server_and_replay_msg(make_finish(car_index));

						c.finished_time = st.tick;
					}
				}
				c.start_lane = c.end_lane;
				auto&p = track.pieces[c.piece_index];
				if (c.next_switch && p.is_switch) {
					c.end_lane = c.start_lane + c.next_switch;
					c.next_switch = 0;
				}
			}
		};

		for (size_t car_index=0;car_index<st.cars.size();++car_index) {
			auto&c = st.cars[car_index];
			
			if (c.lap>=opt.laps) continue;
			if (c.dnf) continue;

			if (st.tick-c.last_passed_start>=opt.max_lap_ticks) {
				client_dnf(car_index);
			}
			if (c.please_dnf && !c.dnf) {
				++st.finished_count;
				c.dnf = true;
				server_and_replay_msg(make_dnf(car_index));
				continue;
			}

			if (c.crashed_until) continue;

			auto&p = track.pieces[c.piece_index];
			double radius = 0;
			if (c.start_lane==c.end_lane) {
				if (p.angle) {
					double lanepos = track.lanes[c.start_lane];
					radius = p.radius + (p.angle<0 ? lanepos : -lanepos);
				}
			} else if (p.angle) {
				auto*sw_piece = lane_switch_pieces[c.piece_index*track.lanes.size()*2+c.start_lane*2+(c.end_lane>c.start_lane ? 1 : 0)];
				if (!sw_piece) xcept("null switch piece");
				double length = get_lane_length(c.piece_index,c.start_lane,c.end_lane);
				size_t idx = (size_t)(c.piece_pos / length * 100.0);
				if (idx>=100) xcept("bad switch radius index %d",idx);
				radius = (*sw_piece)[idx].radius;
			}
			
			double throttle = c.throttle;
			if (c.turbo_until>st.tick && c.turbo_until) throttle *= opt.turbo_factor;
			c.rotspeed += angle_correction(c.speed,c.angle,c.rotspeed);
			if (radius) {
				double slip = angle_slip(c.speed,radius);
				if (slip>0) {
					if (p.angle<0) c.rotspeed -= slip;
					else c.rotspeed += slip;
				}
			}
			c.angle += c.rotspeed;

			double acc = acceleration(throttle,c.speed);
			c.speed += acc;

			if (std::abs(c.angle)<crash_angle) c.piece_pos += c.speed;
			check_piece_transition(car_index);
		}
		++st.tick;

		for (size_t car_index=0;car_index<st.cars.size();++car_index) {
			auto&c = st.cars[car_index];
			if (c.lap>=opt.laps) continue;
			if (c.dnf) continue;
			
			if (std::abs(c.angle)>=crash_angle) {
				server_and_replay_msg(make_crash(car_index));
				c.crashed_until = st.tick + respawn_time;
				c.has_turbo = false;
				c.throttle = 0;
				c.crash_angle = c.abs_angle + (c.angle<0 ? 0 : PI);
				c.angle = 0;
			}

			if (c.crashed_until) {
				if (c.crashed_until==st.tick) {
					server_and_replay_msg(make_spawn(car_index));
					c.crashed_until = 0;
					c.rotspeed = 0;
					c.speed = 0;
				}
				continue;
			}
		}

		for (size_t car_index=0;car_index<st.cars.size();++car_index) {
			auto&c = st.cars[car_index];

			if (c.lap>=opt.laps) continue;
			if (c.dnf) continue;
			if (c.crashed_until || c.angle>=crash_angle) continue;

			double my_pos = get_abs_lane_pos(c.piece_index,c.start_lane) + c.piece_pos;
			for (size_t car2_index=car_index+1;car2_index<st.cars.size();++car2_index) {
				if (car_index==car2_index) continue;
				auto&c2 = st.cars[car2_index];
				if (c2.start_lane!=c.start_lane || c2.end_lane!=c.end_lane) continue;
				if (c2.lap>=opt.laps) continue;
				if (c2.dnf) continue;
				if (c2.crashed_until) continue;
				double n_pos = get_abs_lane_pos(c2.piece_index,c2.start_lane) + c2.piece_pos;
				double d = n_pos - my_pos;
				if (std::abs(d)<cars[0].length) {
					if (d<0) {
						//log("%d was bumped by %d\n",car_index,car2_index);
						double old_speed = c.speed;
						c.speed = c2.speed * 0.9;
						c.piece_pos += (n_pos+cars[0].length) - my_pos;
						c2.speed = old_speed * 0.8;
						check_piece_transition(car_index);

						auto msg_bump = make_bump(car_index,car2_index);
						replay_msg(msg_bump);
						replay_msg(msg_bump);
					} else {
						//log("%d was bumped by %d\n",car2_index,car_index);
						double old_speed = c2.speed;
						c2.speed = c.speed * 0.9;
						c2.piece_pos += (my_pos+cars[0].length) - n_pos;
						c.speed = old_speed * 0.8;
						check_piece_transition(car2_index);

						auto msg_bump = make_bump(car2_index,car_index);
						replay_msg(msg_bump);
						replay_msg(msg_bump);
					}
				}
			}

		}


		if (st.tick-st.last_turbo_available==opt.turbo_interval) {
			for (size_t car_index=0;car_index<st.cars.size();++car_index) {
				auto&c = st.cars[car_index];
				if (c.crashed_until) continue;
				if (c.lap>=opt.laps) continue;
				if (c.dnf) continue;
				c.has_turbo = true;
			}
			server_and_replay_msg(make_turboAvailable());
			st.last_turbo_available = st.tick;
		}

		for (size_t car_index=0;car_index<st.cars.size();++car_index) {
			auto&c = st.cars[car_index];
			if (c.dnf) continue;
			update_abs_values(c);
			if (c.crashed_until) continue;
			if (c.lap>=opt.laps) continue;

			if (c.turbo_until==st.tick) {
				server_and_replay_msg(make_turboEnd(car_index));
				c.turbo_until = 0;
			}

			if (c.has_turbo && c.use_turbo) {
				server_and_replay_msg(make_turboStart(car_index));
				c.turbo_until = st.tick + opt.turbo_ticks + 1;
				c.has_turbo = false;
				c.use_turbo = false;
			}
		}

		server_msg(-1,make_carPositions());

		replay_msg(make_fullCarPositions());
		replay_msg(make_carVelocities());
		if (update_live_ranking() || st.tick%61==1) {
			replay_msg(make_liveRanking());
		}

		if (someone_passed_start) {
			for (size_t car_index=0;car_index<st.cars.size();++car_index) {
				auto&c = st.cars[car_index];
				if (c.last_passed_start!=st.tick-1 || c.lap<=0) continue;

				server_and_replay_msg(make_lapFinished(car_index));
			}
		}

		if (st.finished_count==st.cars.size()) {
			server_and_replay_msg(make_gameEnd());
			server_and_replay_msg(make_tournamentEnd());
			return false;
		}

		return true;
	}

};



}
